#!/usr/bin/python

import cv2
import sys, getopt

helpCommand = 'blur.py -i <inputfile> -o <outputfile> -m <markfile>'

def run_blur(inputfile, outputfile, maskfile, preview):
  ## Import images
  img = cv2.imread(inputfile, -1)
  imgMask = cv2.imread(maskfile, 1)

  ## Blur image using the mask
  ## https://stackoverflow.com/a/54588559
  blurredImg = cv2.blur(img,(30,30),0)
  out = img.copy()
  out[imgMask==0] = blurredImg[imgMask==0]

  ## Preview result
  if preview:
    cv2.imshow('Result preview', out)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

  ## Write result image
  cv2.imwrite(outputfile, out)

def main(argv):
  inputfile = ''
  outputfile = ''
  maskfile = ''
  preview = False

  try:
    opts, args = getopt.getopt(argv,"phi:o:m:",["ifile=","ofile=", "mfile="])
  except getopt.GetoptError:
    print(helpCommand)
    sys.exit(2)
  
  for opt, arg in opts:
    if opt == '-h':
      print(helpCommand)
      print('\t-p to preview result')
      sys.exit()
    elif opt == '-p':
      preview = True
    elif opt in ("-i", "--ifile"):
      inputfile = arg
    elif opt in ("-o", "--ofile"):
      outputfile = arg
    elif opt in ("-m", "--mfile"):
      maskfile = arg
  if not inputfile or not outputfile or not maskfile:
      print(helpCommand)
      sys.exit()
  
  run_blur(inputfile, outputfile, maskfile, preview)


if __name__ == "__main__":
   main(sys.argv[1:])
